const { Router } = require('express');

const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { Fighter } = require('../models/fighter');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res) => {
    const fighters = FighterService.getFighters();
    if (fighters) {
        res.json(fighters);
    } else {
        res.json(404).json({
            error: true,
            message: 'Fighters not found',
        });
    }
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    const foundFighter = FighterService.search({ id });
    if (foundFighter) {
        res.json(foundFighter);
    } else {
        res.status(404).json({ error: true, message: 'Fighter not found' });
    }
});

router.post('/', createFighterValid, (req, res) => {
    const data = new Fighter(req.body);
    const fighter = FighterService.create(data);
    if (fighter) {
        res.json(fighter);
    } else {
        res.status(400).json({
            error: true,
            message: 'Valid in creating fighter',
        });
    }
});

router.put('/:id', updateFighterValid, (req, res) => {
    const id = req.params.id;
    const fighterInfo = new Fighter(req.body);
    const updatedFighter = FighterService.update(id, fighterInfo);
    if (updatedFighter) {
        res.json(updatedFighter);
    } else {
        res.status(404).json({
            error: true,
            message: 'Fighter not found',
        });
    }
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    const deletedFighter = FighterService.delete(id);
    if (deletedFighter) {
        res.json(deletedFighter);
    } else {
        res.status(404).json({
            error: true,
            message: 'Fighter not found',
        });
    }
});

module.exports = router;
