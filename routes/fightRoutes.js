const { Router } = require('express');

const FightService = require('../services/fightService');
const { Fight } = require('../models/fight');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res) => {
    const fights = FightService.getFights();
    if (fights) {
        res.json(fights);
    } else {
        res.json(404).json({
            error: true,
            message: 'There is no fight',
        });
    }
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    const foundFight = FightService.getFightsForUser({ userId: id });
    if (foundFight) {
        res.json(foundFight);
    } else {
        res.status(404).json({
            error: true,
            message: 'There is no fight for this id',
        });
    }
});

router.post('/', (req, res) => {
    const fight = new Fight(req.body);
    const result = FightService.create(fight);
    if (result) {
        res.json(result);
    } else {
        res.status(400).json({
            error: true,
            message: 'Can not crate a fight',
        });
    }
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    const deletedFight = FightService.delete(id);
    if (deletedFight) {
        res.json(deletedFight);
    } else {
        res.status(404).json({
            error: true,
            message: 'There is no fight for this id',
        });
    }
});

module.exports = router;
