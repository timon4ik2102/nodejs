const { Router } = require('express');

const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { User } = require('../models/user');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res) => {
    try {
        const users = UserService.getAll();
        res.json(users);
    } catch (e) {
        res.status(500).json({
            error: true,
            message: 'There is no users in data',
        });
    }
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    try {
        const user = UserService.search({ id });
        if (!user) {
            res.status(404).send({
                error: true,
                message: 'There is no  user with this id',
            });
        }
        res.json(user);
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error',
        });
    }
});

router.post('/', createUserValid, (req, res) => {
    const user = req.body;
    console.log(user);
    try {
        res.json(UserService.create(user));
    } catch (e) {
        res.status(400).json({
            error: true,
            message: 'Internal server error',
        });
    }
});

router.put('/:id', updateUserValid, (req, res) => {
    const id = req.params.id;
    const dataToUpdate = req.body;
    try {
        res.json(UserService.update(id, dataToUpdate));
    } catch (e) {
        res.status(404).json({
            error: true,
            message: 'There is no  user with this id',
        });
    }
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    try {
        const user = UserService.delete(id);
        if (!user) {
            res.status(404).json({
                error: true,
                message: 'We can not delete undefined user',
            });
        }
        res.json(user);
    } catch (e) {
        res.status(500).json({
            error: true,
            message: 'Internal server error',
        });
    }
});

module.exports = router;
