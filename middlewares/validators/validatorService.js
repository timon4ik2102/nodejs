const UserService = require('../../services/userService');

class ValidatorService {
    isSmaller = (value, min) => {
        return Number(value) >= min;
    };

    isBigger = (value, max) => {
        return Number(value) < max;
    };

    isGmailEmail = (value) => {
        return /(\W|^)[\w.+\-]*@gmail\.com(\W|$)/gi.test(value);
    };

    isPhoneNumber = (value) => {
        return /^\+380(\d{9})$/g.test(value);
    };

    numberOfFields = (object, count) => {
        return Object.keys(object).length === count;
    };

    isLength = (value, { min, max }) => {
        if (min) {
            if (max) {
                return value.length >= min && value.lenght < max;
            }
            return value.length >= min;
        }
    };

    uniqueEmail = (email) => {
        return !UserService.search({ email });
    };

    userExists = (id) => {
        return UserService.search(id);
    };
}

module.exports = new ValidatorService();
